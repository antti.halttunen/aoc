* Advent of Code

  [[https://adventofcode.com/]]

** 2020

   [[https://adventofcode.com/2020]]

*** Day 1

    [[https://adventofcode.com/2020/day/1]]

#+begin_src clojure
(defn mc [g n xs]
  (->> (combo/combinations xs n)
       (filter #(= g (reduce + %)))
       (apply reduce *)))
#+end_src

*** Day 2

    [[https://adventofcode.com/2020/day/2]]

#+begin_src clojure
(def policy-regex #"(?<min>[0-9]+)-(?<max>[0-9]+) (?<char>[a-z]+): (?<password>[a-z]+)")

(defn read-policy-line
  [s]
  (let [matcher (re-matcher policy-regex s)]
    (if (.matches matcher)
      {:min (edn/read-string (.group matcher "min"))
       :max (edn/read-string (.group matcher "max"))
       :char (.group matcher "char")
       :password (.group matcher "password")}
      (throw (ex-info (str "parse fail"))))))

(defn check-policy
  [{:keys [min max char password]}]
  (let [ss (str/split password #"")
        cs (filter #{char} ss)
        n (count cs)
        t (and (<= min n) (<= n max))]
    [t min max char n password]))

(defn check-policy-xor
  [{:keys [min max char password]}]
  (let [ss (str/split password #"")
        a (nth ss (dec min))
        b (nth ss (dec max))
        n (count (filter #{char} [a b]))]
    [(= 1 n) min max char n password]))
#+end_src

*** Day 3

    [[https://adventofcode.com/2020/day/3]]

#+begin_src clojure
(defn ys [y v]
  (->> v
       (#(str/split % #""))
       (keep-indexed #(if (= %2 "#") %1 nil))
       (map #(hash-map :y y :x %))))

(defn locs [s] (->> s (map-indexed #(ys %1 %2)) flatten))

(defn ingest [s] {:xs (locs s) :h (count s) :w (-> s first count)})

(defn hit? [m x y] (->> m :xs (some #{{:x (mod x (:w m)) :y y}})))

(defn hits [m xi yi]
  (loop [x 0 y 0 n 0]
    (if (> y (:h m))
      n
      (recur (+ x xi) (+ y yi) (if (hit? m x y) (inc n) n)))))
#+end_src

*** Day 4

    [[https://adventofcode.com/2020/day/4]]

#+begin_src clojure
(defn entry [s]
  (->> s
       (re-seq #"(\w{3}):(\S+)")
       (map next)
       (map vec)
       (into {})))

(defn s->int [s] (edn/read-string s))

(def required-fields ["byr" "iyr" "eyr" "hgt" "hcl" "ecl" "pid"])

(defn valid? [m] (every? #(contains? m %) required-fields))

(defn valid-2? [{:strs [byr iyr eyr hgt hcl ecl pid]}]
  (and
   ;; byr (Birth Year) - four digits; at least 1920 and at most 2002.
   byr (<= 1920 (s->int byr) 2002)
   ;; iyr (Issue Year) - four digits; at least 2010 and at most 2020.
   iyr (<= 2010 (s->int iyr) 2020)
   ;; eyr (Expiration Year) - four digits; at least 2020 and at most 2030.
   eyr (<= 2020 (s->int eyr) 2030)
   ;; hgt (Height) - a number followed by either cm or in:
   ;; If cm, the number must be at least 150 and at most 193.
   ;; If in, the number must be at least 59 and at most 76.
   hgt (let [[_ n unit] (re-find #"(\d+)(cm|in)" hgt)]
         (case unit
           "cm" (<= 150 (s->int n) 193)
           "in" (<=  59 (s->int n)  76)
           false ;; invalid!
           ))
   ;; hcl (Hair Color) - a # followed by exactly six characters 0-9 or a-f.
   hcl (re-find #"^#[0-9a-f]{6}$" hcl)
   ;; ecl (Eye Color) - exactly one of: amb blu brn gry grn hzl oth.
   ecl (#{"amb" "blu" "brn" "gry" "grn" "hzl" "oth"} ecl)
   ;; pid (Passport ID) - a nine-digit number, including leading zeroes.
   pid (re-find #"^[0-9]{9}$" pid)
   ;; cid (Country ID) - ignored, missing or not.
   ))

(defn read-input [f] (->> f slurp (split #"\n\n") (map entry)))
(defn part-1 [f] (->> f read-input (filter valid?)   count))
(defn part-2 [f] (->> f read-input (filter valid-2?) count))
#+end_src

*** Day 5

    [[https://adventofcode.com/2020/day/5]]

#+begin_src clojure
(defn frblr-to-binary [s] (case s  \F 0  \B  1  \L 0  \R 1))

(defn to-dec [zs]
  (->> zs (str/join "") (str "2r") (edn/read-string)))

(defn to-pass [s]
  (let [bs (->> s seq (map frblr-to-binary))]
    {:row (->> bs (take 7) to-dec)
     :col (->> bs (drop 7) to-dec)}))

(defn seat-id [seat] (+ (* 8 (:row seat)) (:col seat)))

(defn my-seat
  [seats]
  (let [ss (->> seats (map seat-id) sort)
        a  (first ss)
        b  (last ss)
        zs (range a b) ;; all possible seats
        hole (set/difference (set zs) (set ss))]
        (first hole)))
#+end_src

Let's do that again.

#+begin_src clojure
(def frbl {\F 0  \B 1  \L 0  \R 1})

(defn bin->dec [bits] (reduce #(+ (* 2 %1) %2) bits))

(defn seat [s]
  (->> s
       (map frbl)
       (partition-all 7)
       (map bin->dec)
       (apply #(+ (* 8 %1) %2))))

(defn read-input [f] (->> f slurp str/split-lines (map seat) sort))

(defn my-seat [seats]
  (->> seats
       (partition-all 2 1)
       (some (fn [[a b]] (when (not= b (inc a)) (inc a))))))
#+end_src


*** Day 6

    [[https://adventofcode.com/2020/day/6]]

#+begin_src clojure
(defn parse-group [s] (str/split-lines s))

(defn any-yes [gp] (->> gp (apply str) distinct count))

(defn all-yes [gp]
  (let [n (count gp)]
    (->> gp (apply str) frequencies (map second) (filter #{n}) count)))

(defn answers [q s] (->> s (split #"\n\n") (map parse-group) (map q) (reduce +)))

(defn part-1 [f] (->> f slurp (answers any-yes)))
(defn part-2 [f] (->> f slurp (answers all-yes)))
#+end_src

*** Day 8

    [[https://adventofcode.com/2020/day/8]]

#+begin_src clojure
(defn split [regex s] (str/split s regex))

(defn read-entry
  [s]
  (let [[ins arg] (split #" " s)]
    [(keyword ins) (edn/read-string arg)]))

(defn step [acc pc prog]
  (let [[ins arg] (nth prog pc)]
    (case ins
      :nop [acc         (inc pc)   prog]
      :acc [(+ acc arg) (inc pc)   prog]
      :jmp [acc         (+ pc arg) prog])))

(defn mutate
  [prog n]
  (loop [n n]
    (let [[ins arg] (nth prog n)]
      (cond
        (= :jmp ins)                    [n (assoc prog n [:nop arg])]
        (and (= :nop ins) (not= 0 arg)) [n (assoc prog n [:jmp arg])]
        :else (recur (inc n))))))

(defn run-or-die
  [prog]
  (loop [acc 0
         pc  0
         old 0
         visited #{}]
    (cond
      (get visited pc)     [false old]
      (<= (count prog) pc) [true  acc]
      :else (let [[nacc npc fu] (step acc pc prog)]
              (recur nacc npc acc (conj visited pc))))))

(defn terminates?      [prog] (->> prog run-or-die first))
(defn stop-at-infinity [prog] (->> prog run-or-die second))
(defn run-to-end       [prog] (->> prog run-or-die second))

(defn hack-the-machine
  [prog]
  (loop [n 0]
    (let [[nn mutated] (mutate prog n)]
      (if (terminates? mutated)
        (run-to-end mutated)
        (recur (inc nn))))))
#+end_src

*** Day 9

    [[https://adventofcode.com/2020/day/9]]

#+begin_src clojure
(defn sus? [x ns]
  (->> (combo/combinations ns 2)
       (filter #(= x (reduce + %)))
       count zero?))

(defn suspect [n nums]
  (loop [pres (take n nums)
         nums (drop n nums)]
    (if (sus? (first nums) pres)
      (first nums)
      (recur (concat (rest pres) [(first nums)])
             (rest nums)))))

(defn constituents [x nums]
  (loop [hs  []
         ts  nums]
    (let [sum (reduce + hs)]
      (cond
        (= x sum) hs
        (< x sum) (recur (rest   hs)                    ts)
        :else     (recur (concat hs [(first ts)]) (rest ts))))))

(defn find-range [n nums]
  (let [x  (suspect n nums)
        cs (constituents x nums)
        ss (sort cs)]
    (+ (first ss) (last ss))))
#+end_src

*** Day 12

    [[https://adventofcode.com/2020/day/12]]

#+begin_src clojure
(def origo {:x 0 :y 0})
(defn read-is [s]
  (->> s
       (re-seq #"(\w{1})(\d+)")
       (map next)
       (map (fn [[i n]] [(keyword i)(edn/read-string n)]))))

(defn abs [n] (max n (- n)))
(defn manhattan [pos] (->> (vals pos) (map abs) (reduce +)))

(defn rot90-cw [{:keys [x y]}] {:x y :y (- 0 x)})
(defn rot-cw [degs wp]
  (case degs
    0  wp
    90 (rot90-cw wp)
    (recur (- degs 90) (rot90-cw wp))
    ))
(defn rot-ccw [degs wp] (rot-cw (- 360 degs) wp))

(defn turn-left  [degrees heading] (mod (- heading degrees) 360))
(defn turn-right [degrees heading] (turn-left (- degrees) heading))

(defn move-forward
  [n direction position]
  (case direction
    0   (update position :x #(- % n))
    180 (update position :x #(+ % n))
    90  (update position :y #(+ % n))
    270 (update position :y #(- % n))))

(defn shift [a n b k] (update a k #(+ % (* n (k b)))))
(defn towards-wp [waypoint n ship]
  (->> (keys ship)
       (reduce #(shift %1 n waypoint %2) ship)))

(defn run [f st is] (reduce #(f %2 %1) st is))

(defn act-1
  [[i n] {:keys [hdng] :as st}]
  (case i
    :N (update-in st [:pos :x] #(- % n))
    :S (update-in st [:pos :x] #(+ % n))
    :E (update-in st [:pos :y] #(+ % n))
    :W (update-in st [:pos :y] #(- % n))
    :F (update-in st [:pos]    #(move-forward n hdng %))
    :L (update-in st [:hdng]   #(turn-left n %))
    :R (update-in st [:hdng]   #(turn-right n %))))

(defn act-2
  [[i n] {:keys [wp] :as st}]
  (case i
    :N (update-in st [:wp :x] #(- % n))
    :S (update-in st [:wp :x] #(+ % n))
    :E (update-in st [:wp :y] #(+ % n))
    :W (update-in st [:wp :y] #(- % n))
    :L (update-in st [:wp]    #(rot-ccw n %))
    :R (update-in st [:wp]    #(rot-cw n %))
    :F (update-in st [:pos]   #(towards-wp wp n %))))

(defn common [f fn st] (->> f slurp read-is (run fn st) :pos manhattan))
(defn part-1 [f] (common f act-1 {:pos origo :hdng 90}))
(defn part-2 [f] (common f act-2 {:pos origo :wp {:x -1 :y 10} }))
#+end_src

*** Day 16

    [[https://adventofcode.com/2020/day/16]]

#+begin_src clojure
(def rule-regex #"^([ \w]+): (\d+)-(\d+) or (\d+)-(\d+)")

(defn fits?     [v r] (and (<= (first r) v) (<= v (second r))))
(defn is-valid? [rule value] (some #(fits? value (% rule)) [:a :b]))

(defn read-rule [s]
  (let [[_ name r1 r2 r3 r4] (re-find rule-regex s)]
    {:name name
     :a (map edn/read-string [r1 r2])
     :b (map edn/read-string [r3 r4])}))

(defn read-rules     [s] (->> s str/split-lines (map read-rule)))
(defn read-nums      [s] (->> s (split #",") (map edn/read-string)))
(defn read-my-ticket [s] (->> s str/split-lines last read-nums))
(defn read-tickets   [s] (->> s str/split-lines (drop 1) (map read-nums)))
(defn read-notes     [s]
  (let [[rules mine tkts] (split #"\n\n" s)]
    {:rules (read-rules rules)
     :mine  (read-my-ticket mine)
     :tkts  (read-tickets tkts)}))

(defn some-match [rules value] (some #(is-valid? % value) rules))

(defn invalids [rules ticket] (->> ticket (remove #(some-match rules %))))

(defn error-rate [{:keys [rules tkts]}]
  (->> tkts (map #(invalids rules %)) flatten (reduce +)))

(defn valids [{:keys [rules tkts]}]
  (->> tkts (filter #(empty? (invalids rules %)))))

(defn ifields [tkts] (->> tkts (apply mapv vector) (map-indexed vector)))

(defn match-fields [r fields]
  (->> fields
       (filter #(every? (fn [v] (is-valid? r v)) (second %)))
       (map first)))

(defn order [st]
  (loop [rules  (:rules st)
         fields (->> st valids ifields)
         must   []]
    (if (empty? rules)
      (->> must (sort-by first) (map second) (map :name))
      (let [r (first rules)
            is (match-fields r fields)]
        (if (< 1 (count is))
          (recur (conj (vec (rest rules)) r)
                 fields
                 must)
          (recur (rest rules)
                 (remove #(= (first is) (first %)) fields)
                 (conj must [(first is) r])))))))

(defn my-deps [{:keys [mine tkts] :as st}]
  (->> (map vector (order st) mine)
       (filter #(re-matches #"departure.*" (first %)))
       (map second)
       (reduce *)))

(defn part-1 [f] (->> f slurp read-notes error-rate))
(defn part-2 [f] (->> f slurp read-notes my-deps))
#+end_src

*** Day 17

    [[https://adventofcode.com/2020/day/17]]

#+begin_src clojure
(defn read-input-2d [s] (->> s str/split-lines str-to-locs         set))
(defn read-input-3d [s] (->> s read-input-2d (map #(assoc % :z 0)) set))
(defn read-input-4d [s] (->> s read-input-3d (map #(assoc % :w 0)) set))

(defn neighbors-3d [{:keys [x y z]}]
  (for [xi [0 -1 1]
        yi [0 -1 1]
        zi [0 -1 1]
        :when (not= [xi yi zi] [0 0 0])]
    {:x (+ xi x) :y (+ yi y) :z (+ zi z)}))

(defn neighbors-4d [{:keys [x y z w]}]
  (for [xi [0 -1 1]
        yi [0 -1 1]
        zi [0 -1 1]
        wi [0 -1 1]
        :when (not= [xi yi zi wi] [0 0 0 0])]
     {:x (+ xi x) :y (+ yi y) :z (+ zi z) :w (+ wi w)}))

(declare neighbors)
(declare read-input)

(defn all-space [ss]
  (->> ss (map neighbors) (map set) (apply set/union ss)))

(defn deactivate? [a actives]
  (let [nbrs (set (neighbors a))
        acts (set/intersection nbrs actives)]
    (not (<= 2 (count acts) 3))))

(defn activate? [i actives]
  (let [nbrs (set (neighbors i))
        acts (set/intersection actives nbrs)]
    (= 3 (count acts))))

(defn toggle-actives-off [actives]
  (->> (vec actives) (filter #(deactivate? % actives)) set))

(defn toggle-inactives-on [actives]
  (->> (set/difference (all-space actives) actives)
       (filter #(activate? % actives))
       set))

(defn step [st]
    (set/union (toggle-inactives-on st)
               (set/difference st (toggle-actives-off st))))

(defn sim [f] (->> f slurp read-input (iterate step) (drop 6) first count))

(defn part-1 [f]
  (def read-input read-input-3d)
  (def neighbors neighbors-3d)
  (sim f))

(defn part-2 [f]
  (def read-input read-input-4d)
  (def neighbors  neighbors-4d)
  (sim f))
#+end_src

** 2021

*** library functions

#+name: lib-funcs
#+begin_src racket
  #lang rackjure
  (define (flip f a b) (f b a))
  (define (lines s) (string-split s "\n"))
  (define (sections s) (string-split s "\n\n"))
  (define (split sep s) (string-split s sep ))
  (define (strs-nums lst) (~>> lst (map #λ(string->number %))))
  (define (splode s) (~>> s string->list (map string)))
  (define (csv s) (~>> s (split ",") strs-nums))
  (define (partition-step n s lst)
    (if (> n (length lst))
        '()
        (cons (take lst n) (partition-step n s (drop lst s)))))
  (define (partition n lst) (partition-step n n lst))
  (define (sum lst) (apply + lst))
  (define (transpose xss) (apply map list xss))
  (define (nth as n) (if (= 0 n) (first as) (nth (rest as) (- n 1))))
  (define (nh n xs) (nth xs n))
  (define (up m k f) (dict-update m k f))
  (define (indict d) (foldl #λ(dict-set %2 (dict-ref d %) %) {} (dict-keys d)))
  (define (tk n xs) (take xs n))
  (define (dp n xs) (drop xs n))
  (define (rep n f i) (if (= n 0) i (rep (sub1 n) f (f i) )))
  (define (irange a b) (range a (add1 b)))
  (define (all? p xs) (foldl #λ(and %2 (p %)) #t xs))
  (define (any? p xs) (foldl #λ(or %2 (p %)) #f xs))
  (define (all-xys X Y) (for*/list ([x (in-range 0 X)] [y (in-range 0 Y)]) {'x x 'y y}))
  (define (found? v lst) (~>> lst (member v) (eq? #f) not))
#+end_src

*** Day 1

[[https://adventofcode.com/2021/day/1]]

#+begin_src racket :lang lazy :var input-file="2021/1-input.txt"
  (require rackjure)
  (define (strs-nums lst) (~>> lst (map (lambda (x) (string->number x)))))
  (define input (~>> input-file file->lines strs-nums))
  (define (sum lst) (apply + lst))
  (define (bigger? a b) (> (sum a) (sum b)))
  (define (push i lst) (~> (cons i lst) (drop-right 1)))
  (define (f kur acc)
    (let* ([prev (dict-ref acc 'prev)]
           [nxt  (push kur prev)]
           [n    (dict-ref acc 'n)]
           [x    (if (bigger? nxt prev) (add1 n) n)])
      {'prev nxt 'n x}))
  (define (slider n lst)
    (~> (foldl f {'prev (reverse (take lst n)) 'n 0} (drop lst n))
        (dict-ref 'n)))
  (slider 1 input)
  (slider 3 input)
#+end_src

*** Day 2

[[https://adventofcode.com/2021/day/2]]

#+begin_src racket :lang racket :var input-file="2021/2-input.txt" :noweb yes
  (require rackjure)
  (define instr-r #rx"^([a-z]*) ([0-9]*)")
  (define (line-d s) (regexp-match instr-r s))
  (define (lines-data lst) (~>> lst (map (lambda (x) (line-d x)))))
  (define input (~>> input-file file->lines lines-data))
  (define (adjust-by-aim m n)
    (let* ([a (dict-update m 'horiz (lambda (i) (+ i n)))]
           [aim (dict-ref m 'aim)])
      (dict-update a 'depth (lambda (i) (+ i (* aim n))))))
  (define (f1 kur acc)
    (let* ([n (string->number (third kur))])
      (case (second kur)
        [("up")      (dict-update acc 'depth (lambda (i) (- i n)))]
        [("down")    (dict-update acc 'depth (lambda (i) (+ i n)))]
        [("forward") (dict-update acc 'horiz (lambda (i) (+ i n)))])))
  (define (f2 kur acc)
    (let* ([n (string->number (third kur))])
      (case (second kur)
        [("up")      (dict-update acc 'aim (lambda (i) (- i n)))]
        [("down")    (dict-update acc 'aim (lambda (i) (+ i n)))]
        [("forward") (adjust-by-aim acc n)])))
  (define (ans f instrs)
    (let ([res (foldl f {'depth 0 'horiz 0 'aim 0} instrs)])
      (* (dict-ref res 'depth) (dict-ref res 'horiz))))
  (ans f1 input)
  (ans f2 input)
#+end_src

*** Day 3

[[https://adventofcode.com/2021/day/3]]

#+begin_src racket :noweb yes
  #lang rackjure
  (define (to-bits s) (~>> s string->list (map string) (map string->number)))
  (define (to-dec bits) (~> (map str bits) (string-join "") (string->number 2)))
  (define (nth as n) (if (= 0 n) (first as) (nth (rest as) (- n 1))))
  (define (combo f as bs) (foldr #λ(cons (f % %2) %3) '() as bs))
  (define (up m k f) (dict-update m k f))
  (define input-file "2021/3-input.txt")
  (define input-data (~>> input-file file->lines (map to-bits)))
  (define (mul a b) (* (to-dec a) (to-dec b)))
  (define (plus as bs) (combo + as bs))
  (define (stats as)
    (let ([n (length as)]
          [ns (foldl plus (first as) (rest as))])
      (map #λ(/ % n) ns)))
  (define (p1 data)
    (let* ([s (stats data)]
           [a (map #λ(if (>= % 0.5) 1 0) s)]
           [b (map #λ(if (>= % 0.5) 0 1) s)])
      (mul a b)))
  (define (categorize kur acc p)
    (if (p (floor (+ 0.5 (nth (acc 'stats) (acc 'pos))))
           (nth kur           (acc 'pos)))
        (up acc 'keep #λ(cons kur %))
        acc))
  (define (mostest  kur acc) (categorize kur acc #λ(     = % %2)))
  (define (leastest kur acc) (categorize kur acc #λ(not (= % %2))))
  (define (prune f data pos)
    (if (= 1 (length data))
        (first data)
        (let ([res (foldl f {'keep '() 'stats (stats data) 'pos pos} data)])
          (prune f ('keep res) (add1 pos)))))
  (define (p2 data)
    (let ([a (prune mostest  data 0)]
          [b (prune leastest data 0)])
      (mul a b)))
  (p1 input-data)
  (p2 input-data)
#+end_src

*** Day 4

[[https://adventofcode.com/2021/day/4]]

#+begin_src racket :noweb yes
  <<lib-funcs>>
  (define input-file "2021/4-input.txt")
  (define input-string (~>> input-file file->string))
  (define (nums-get s) (~> s lines first (string-split ",") strs-nums))
  (define (s-to-boards s) (~> s lines string-join (string-split #:trim? #t) strs-nums))
  (define (board-lists s) (~>> (~> s sections (drop 1)) (map #λ(s-to-boards %))))
  (define (rows board) (~>> (partition 5 board)           (apply map set)))
  (define (cols board) (~>> (partition 5 board) transpose (apply map set)))
  (define (board-sets s) (~>> s board-lists (map #λ(append (cols %) (rows %)))))
  (define (wins? board nums) (~>> board (filter #λ(subset? % (apply set nums))) empty? not))
  (define (winner boards nums n)
    (let ([ws (filter #λ(wins? % (take nums n)) boards)])
      (case (length ws)
        [(1)  {'n n 'nums nums 'w (first ws)}]
        [else (winner boards nums (add1 n))])))
  (define (loser boards nums n)
    (let ([ws (filter #λ(wins? % (take nums n)) boards)]
          [ls (filter-not #λ(wins? % (take nums n)) boards)])
      (case (length ls)
        [(0)  {'n n 'nums nums 'w (first ws)}]
        [else (loser ls nums (add1 n))])))
  (define (driver f s n) (f (board-sets s) (nums-get s) n))
  (define (solver f s)
      (let* ([res (driver f s 5)]
             [ns  (take ('nums res) ('n res))]
             [m   (last ns)]
             [xs  (set-subtract (apply set-union ('w res)) (apply set ns))]
             [sum (~> xs set->list sum)])
        (* m sum)))
  (solver winner input-string)
  (solver loser  input-string)
#+end_src

*** Day 5

[[https://adventofcode.com/2021/day/5]]

#+begin_src racket :noweb yes
  <<lib-funcs>>
  (define input-file "2021/5-input.txt")
  (define input-lines (~>> input-file file->lines))
  (define ab-r #rx"^([0-9]*),([0-9]*) -> ([0-9]*),([0-9]*)")
  (define (ab s)
    (let* ([ab (~>> (regexp-match ab-r s) (dp 1) (map string->number))])
      {'a {'x (first ab) 'y (second ab)} 'b {'x (third ab) 'y (fourth ab)}}))
  (define (hor-ver? ab) (or (= (~> ab 'a 'x) (~> ab 'b 'x)) (= (~> ab 'a 'y) (~> ab 'b 'y))))
  (define (inc from to) (if (= from to) from (if (< from to) (add1 from) (sub1 from))))
  (define (towards a b) {'x (inc (~> a 'x) (~> b 'x)) 'y (inc (~> a 'y) (~> b 'y))})
  (define (span-i a b res) (if (equal? a b) (cons a res) (span-i (towards a b) b (cons a res))))
  (define (span ab) (span-i (~> ab 'a) (~> ab 'b) '()))
  (define (count-overlaps prune lines)
    (~>> lines (map ab) (filter prune) (map span) (apply append)
         (group-by identity) (filter #λ(< 1 (length %))) length))
  (~>> input-lines (count-overlaps hor-ver?))
  (~>> input-lines (count-overlaps identity))
#+end_src

*** Day 6

[[https://adventofcode.com/2021/day/6]]

#+begin_src racket :noweb yes
  <<lib-funcs>>
  (define input-file "2021/6-input.txt")
  (define input-string (~> input-file file->string lines first))
  (define blanco (foldl #λ(dict-set %2 % 0) {} (range 0 9)))
  (define (canon s) (~>> s (split ",") strs-nums (foldl #λ(up %2 % add1) blanco)))
  (define (sim st i acc)
    (let ([f #λ(+ % (~> st i))])
      (case i
        [(0)  (~> acc (up 6 f) (up 8 f))]
        [else (~> acc (up (sub1 i) f))])))
  (define (step st) (foldl #λ(sim st % %2) blanco (dict-keys st)))
  (~>> input-string canon (rep  80 step) (dict-values) sum)
  (~>> input-string canon (rep 256 step) (dict-values) sum)
#+end_src

*** Day 7

[[https://adventofcode.com/2021/day/7]]

#+begin_src racket :noweb yes
  <<lib-funcs>>
  (define input-file "2021/7-input.txt")
  (define input-data (~>> input-file file->string lines first (split ",") strs-nums))
  (define (exp1 n) n)
  (define (exp2 n) (/ (* n (add1 n)) 2))
  (define (cost f p x) (~> (- p x) abs f))
  (define (fuel f p xs) (~>> xs (map #λ(cost f p %)) sum))
  (define (cheapest f ns)
    (~>> (range (apply min ns) (apply max ns))
         (map #λ(fuel f % ns))
         (flip sort <)
         first))
  (cheapest exp2 input-data)
  (cheapest exp1 input-data)
#+end_src

*** Day 8

[[https://adventofcode.com/2021/day/8]]

#+begin_src racket :noweb yes
   <<lib-funcs>>
   (define seq->num {"abcefg" 0 "cf" 1 "acdeg" 2 "acdfg" 3 "bcdf" 4 "abdfg" 5 "abdefg" 6 "acf" 7 "abcdefg" 8 "abcdfg" 9})
   (define (one? s)    (= 2 (string-length s)))
   (define (seven? s)  (= 3 (string-length s)))
   (define (four? s)   (= 4 (string-length s)))
   (define (eight? s)  (= 7 (string-length s)))
   (define input-file "2021/8-input.txt")
   (define io-r #rx"^([a-z ]*) \\| ([a-z ]*)")
   (define (line-d s) (regexp-match io-r s))
   (define (io s)
     (let* ([m (~>> s line-d (flip drop 1))]
            [inputs  (~>> m first (split " "))]
            [outputs (~>> m second (split " "))])
       {'i inputs 'o outputs}))
   (define input-data (~>> input-file file->string lines (map io)))
   (define (is-unique? s) (or (one? s) (seven? s) (four? s) (eight? s)))
   (define (occurs n xs) (~>> xs (map string->list) (apply append) (group-by identity) (filter #λ(= n (length %))) first first string))
   (define (missing? x xs) (not (~>> xs splode (member x)) ))
   (define (missing-v? m k s) (missing? (~> m k) s))
   (define (find-the-one-not-mentioned as bs) (~>> as splode (filter-not #λ(member % (splode bs))) first))
   (define (not-found-yet s m) (~>> s splode (filter-not #λ(member % (dict-values m))) first))
   (define (f-e is m) (dict-set m "e" (occurs 4 is)))
   (define (f-f is m) (dict-set m "f" (occurs 9 is)))
   (define (f-c is m)
     (let* ([one (findf one? is)]
            [f   (dict-ref m "f")]
            [c   (~>> one splode (findf #λ(not (equal? % f))))])
       (dict-set m "c" c)))
   (define (f-a is m) (dict-set m "a" (not-found-yet (findf seven? is) m)))
   (define (f-d is m)
     (let* ([zero (~>> is
                       (filter #λ(= 6 (string-length %)))
                       (filter-not #λ(missing-v? m "e" %))
                       (filter-not #λ(missing-v? m "c" %))
                       first)]
            [d  (find-the-one-not-mentioned "abcdefg" zero)])
       (dict-set m "d" d)))
  (define (f-b is m) (dict-set m "b" (not-found-yet (findf four? is) m)))
  (define (f-g is m)
    (let* ([g  (not-found-yet "abcdefg" m)])
      (dict-set m "g" g)))
  (define (deduce is) (~>> {} (f-e is) (f-f is) (f-c is) (f-a is) (f-d is) (f-b is) (f-g is)))
  (define (decode k s)
    (~>> s splode (map #λ(dict-ref k %)) (flip sort string<?) (apply string-append)
         (dict-ref seq->num)))
  (define (decrypt fu)
    (let ([x (~>> fu 'i deduce indict)])
      (~>> (~> fu 'o) (map #λ(decode x %)) (map number->string) (apply string-append) string->number)))
  (~>> input-data (map #λ(~> % 'o)) (apply append) (filter is-unique?) length)
  (~>> input-data (map decrypt) sum)
#+end_src

*** Day 9

[[https://adventofcode.com/2021/day/9]]

#+begin_src racket :noweb yes
  <<lib-funcs>>
  (define input-file "2021/9-input.txt")
  (define input-data (~>> input-file file->string lines (map splode) (map strs-nums)))
  (define (all-xys-t tbl) (let ([X (length (first tbl))] [Y (length tbl)]) (all-xys X Y)))
  (define (dig tbl p) (~>> tbl (nh (~> p 'y)) (nh (~> p 'x))))
  (define (orthogonals p X Y)
    (let ([x (~> p 'x)]
          [y (~> p 'y)])
      (for*/list ([xx (list (sub1 x) x (add1 x))] ;; 3 x coords
                  [yy (list (sub1 y) y (add1 y))] ;; 3 y coords
                  #:when (or (equal? x xx) (equal? y yy))       ;; but only orthogonal
                  #:when (not (and (equal? x xx) (equal? y yy))) ;; and not the original
                  #:when (<= 0 xx)  ;; bounded
                  #:when (<= 0 yy)  ;; bounded
                  #:when (< xx X)   ;; bounded
                  #:when (< yy Y))  ;; bounded
        {'x xx 'y yy})))
  (define (neighbors p tbl) (let ([X (length (first tbl))] [Y (length tbl)]) (orthogonals p X Y)))
  (define (is-smallest? p tbl) (all? #λ(< (dig tbl p) (dig tbl %)) (neighbors p tbl)))
  (define (lows tbl) (foldl #λ(if (is-smallest? % tbl) (cons % %2) %2) '() (all-xys-t tbl)))
  (define (risk tbl) (~>> (lows tbl) (map #λ(dig tbl %)) (map add1) sum))
  (define (nb-s p tbl) (apply set (neighbors p tbl)))
  (define (nb-ps ps tbl) (~>> ps set->list (map #λ(nb-s % tbl)) (apply set-union)))
  (define (not-nine? tbl p) (not (= 9 (dig tbl p))))
  (define (not-nines tbl st) (~>> st set->list (filter #λ(not-nine? tbl %)) (apply set)))
  (define (basin tbl kur seen)
    (let* ([all-neighbors (nb-ps kur tbl)]
           [new-neighbors (set-subtract all-neighbors seen)]
           [non-nines     (not-nines tbl new-neighbors)]
           [nxt           (set-union non-nines kur)])
      (if (set-empty? new-neighbors) kur (basin tbl nxt all-neighbors))))
  (define (basins tbl) (~>> tbl lows (map #λ(basin tbl (set %) (set %))) (map #λ(set-count %)) (flip sort >) (flip take 3) (foldl * 1)))
  (risk input-data)
  (basins input-data)
#+end_src

*** Day 10

[[https://adventofcode.com/2021/day/10]]

#+begin_src racket :noweb yes
  <<lib-funcs>>
  (define input-file "2021/10-input.txt")
  (define pairs { "(" ")" "{" "}" "[" "]" "<" ">" })
  (define points { ")" 3 "]" 57 "}" 1197 ">" 25137})
  (define corrs { ")" 1 "]" 2 "}" 3 ">" 4})
  (define (opener? k) (found? k (dict-keys pairs)))
  (define (closes? a b) (equal? a (dict-ref pairs b)))
  (define (chk s q)
    (if (empty? s)
        {'ok #t 's s 'q q}
        (if (opener? (first s))
            (chk (rest s) (cons (first s) q))
            (if (and (not (empty? q)) (closes? (first s) (first q)))
                (chk (rest s) (rest q))
                {'ok #f 'q q 's s}))))
  (define (run s) (chk (splode s) '() ))
  (define (middle xs)
    (let ([n (~> xs length (/ 2) floor)])
      (~>> xs (flip drop n) first)))
  (define (matcher xs) (map  #λ(dict-ref pairs %) xs))
  (define (calc1 xs) (~>> xs (map #λ(dict-ref points %)) sum))
  (define (calc2 xs) (foldl #λ(+ (* 5 %2) (dict-ref corrs %)) 0 xs))
  (define (ok? d) (dict-ref d 'ok))
  (define (pt1 is)
    (~>> is (map run) (filter-not ok?)
         (map #λ(dict-ref % 's)) (map first) calc1))
  (define (pt2 is)
    (~>> is (map run) (filter ok?)
         (map #λ(dict-ref % 'q)) (map matcher) (map calc2) (flip sort <) middle))
  (~>> input-file file->string lines pt1)
  (~>> input-file file->string lines pt2)
#+end_src
